using System;
using LiteDBPortal.Services;
using Xunit;
using Xunit.Abstractions;

namespace LiteDBPortalTest.Services
{
    public class QueryParsingServiceTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public QueryParsingServiceTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Theory]
        [InlineData("SELECT $ FROM Collection1;", "Collection1")]
        [InlineData("SELECT $ FROM Collection1", "Collection1")]
        [InlineData("SELECT $ FROM  Collection1  ", "Collection1")]
        [InlineData("SELECT $ FROM  Collection1 WHERE X > 7", "Collection1")]
        [InlineData("SELECT $ FROM  Collection1\n  WHERE X > 7", "Collection1")]
        public void TestGetCollectionName(string query, string expected)
        {
            var actual = QueryParsingService.ExtractCollectionName(query);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("SELECT DISTINCT($.name) FROM Col1", "SELECT DISTINCT($.name) FROM Col1", 10, 20)]
        [InlineData("SELECT  DISTINCT ($.name) FROM Col1", "SELECT  DISTINCT ($.name) FROM Col1", 10, 20)]
        public void TestQueryForDistinct(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ INTO c1 FROM $file_json('NamesToImport.json');", "SELECT $ INTO c1 FROM $file_json('NamesToImport.json');", 10, 20)]
        public void TestForFileQuery(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT COUNT($.name) FROM Col1", "SELECT COUNT($.name) FROM Col1", 10, 20)]
        [InlineData("SELECT  COUNT ($.name) FROM Col1", "SELECT  COUNT ($.name) FROM Col1", 10, 20)]
        public void TestQueryForCount(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("EXPLAIN SELECT $ FROM Col1", "EXPLAIN SELECT $ FROM Col1", 10, 20)]
        [InlineData("EXPLAIN SELECT $ FROM Col1", "EXPLAIN SELECT $ FROM Col1", 10, 20)]
        public void TestQueryForExplain(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ FROM Col1;", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("select $ from Col1", "select $ from Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1\n", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        public void TestSimpleQueries(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ FROM Col1 WHERE X>7;", "SELECT $ FROM Col1 WHERE X>7 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE X>7", "SELECT $ FROM Col1 WHERE X>7 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("select $ from Col1 where X>7", "select $ from Col1 where X>7 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE X BETWEEN 7 AND 8", "SELECT $ FROM Col1 WHERE X BETWEEN 7 AND 8 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 \n\n WHERE X BETWEEN 7 AND 8", "SELECT $ FROM Col1 \n\n WHERE X BETWEEN 7 AND 8 LIMIT 20 OFFSET 10", 10, 20)]
        public void TestQueryWithWhereClauseQueries(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }
        
        [Theory]
        [InlineData("SELECT $ FROM Col1 WHERE X>7 ORDER BY Y;", "SELECT $ FROM Col1 WHERE X>7 ORDER BY Y LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE X>7 ORDER BY Y", "SELECT $ FROM Col1 WHERE X>7 ORDER BY Y LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("select $ from Col1 where X>7 ORDER BY Y", "select $ from Col1 where X>7 ORDER BY Y LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE X BETWEEN 7 AND 8 ORDER BY Y", "SELECT $ FROM Col1 WHERE X BETWEEN 7 AND 8 ORDER BY Y LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 \n WHERE X BETWEEN 7 AND 8 \n\n ORDER BY Y", "SELECT $ FROM Col1 \n WHERE X BETWEEN 7 AND 8 \n\n ORDER BY Y LIMIT 20 OFFSET 10", 10, 20)]
        public void TestQueryWithWhereClauseOrderByQueries(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ FROM Col1 LIMIT 5;", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 LIMIT 5", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("select $ from Col1 limit 5", "select $ from Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1   LIMIT  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 \n LIMIT  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        public void TestSimpleQueriesWithLimitAlreadyAdded(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ FROM Col1 OFFSET 5;", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 OFFSET 5", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("select $ from Col1 offset 5", "select $ from Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1   OFFSET  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 \n OFFSET  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        public void TestSimpleQueriesWithOffsetAlreadyAdded(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ FROM Col1 LIMIT 10 OFFSET 5;", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 LIMIT 10 OFFSET 5", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("select $ from Col1 limit 10 offset 5", "select $ from Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 LIMIT  10   OFFSET  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 \n LIMIT 10  \n OFFSET  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 OFFSET 10 LIMIT 5;", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 OFFSET 10 LIMIT 5", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("select $ from Col1 offset 10 limit 5", "select $ from Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 OFFSET  10   LIMIT  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 \n OFFSET 10  \n LIMIT  5  ", "SELECT $ FROM Col1 LIMIT 20 OFFSET 10", 10, 20)]
        public void TestSimpleQueriesWithOffsetAndLimitAlreadyAdded(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }
        
        [Theory]
        [InlineData("SELECT $ FROM Offset", "SELECT $ FROM Offset LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Limit", "SELECT $ FROM Limit LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM OFFSET", "SELECT $ FROM OFFSET LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM LIMIT", "SELECT $ FROM LIMIT LIMIT 20 OFFSET 10", 10, 20)]
        public void TestSimpleQueryOffsetAndLimitInCollectionName(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }
        
        [Theory]
        [InlineData("SELECT $ FROM Col1 WHERE Offset > 3", "SELECT $ FROM Col1 WHERE Offset > 3 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE Offset BETWEEN 3 and 4", "SELECT $ FROM Col1 WHERE Offset BETWEEN 3 and 4 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE offset > 3", "SELECT $ FROM Col1 WHERE offset > 3 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE Limit > 3", "SELECT $ FROM Col1 WHERE Limit > 3 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE Limit BETWEEN 3 and 4", "SELECT $ FROM Col1 WHERE Limit BETWEEN 3 and 4 LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 WHERE Limit > 3", "SELECT $ FROM Col1 WHERE Limit > 3 LIMIT 20 OFFSET 10", 10, 20)]
        public void TestSimpleQueryOffsetAndLimitInWhereClause(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ FROM Col1 ORDER BY Offset", "SELECT $ FROM Col1 ORDER BY Offset LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 ORDER BY offset", "SELECT $ FROM Col1 ORDER BY offset LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 ORDER BY offset ASC", "SELECT $ FROM Col1 ORDER BY offset ASC LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 \n ORDER BY offset ASC", "SELECT $ FROM Col1 \n ORDER BY offset ASC LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 ORDER BY Limit", "SELECT $ FROM Col1 ORDER BY Limit LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 ORDER BY Limit", "SELECT $ FROM Col1 ORDER BY Limit LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Col1 ORDER BY Limit ASC", "SELECT $ FROM Col1 ORDER BY Limit ASC LIMIT 20 OFFSET 10", 10, 20)]
        public void TestSimpleQueryOffsetAndLimitInOrderByClause(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

        [Theory]
        [InlineData("SELECT $ FROM Offset WHERE Limit > 7 AND Offset < 8 ORDER BY Offset ASC", "SELECT $ FROM Offset WHERE Limit > 7 AND Offset < 8 ORDER BY Offset ASC LIMIT 20 OFFSET 10", 10, 20)]
        [InlineData("SELECT $ FROM Offset WHERE Limit > 7 AND Offset < 8 ORDER BY Offset,Limit ASC LIMIT 100 OFFSET 7", "SELECT $ FROM Offset WHERE Limit > 7 AND Offset < 8 ORDER BY Offset,Limit ASC LIMIT 20 OFFSET 10", 10, 20)]
        public void TestFullQueryOffsetLimitEverywhere(string originalQuery, string expectedResult, int offset, int limit)
        {
            var actual = QueryParsingService.GenerateQueryWithPageOffsetLimit(originalQuery, offset, limit);
            Assert.Equal(expectedResult, actual);
        }

    }
}