using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using DynamicData;
using LiteDB;
using LiteDBPortal.Generator;
using ReactiveUI;

namespace LiteDBPortal.ViewModels
{
    public class DatabaseViewModel : ViewModelBase, IDisposable
    {
        private const string UnknownValueString = "<unknown>";
        private bool _disposed = false;

        public ObservableCollection<DbPropertyTreeNodeViewModel> Properties { get; }
        
        public ReactiveCommand<Unit,Unit> CloseDatabaseCmd { get; }
        
        public LiteDatabase Database { get; }
        
        public string Filepath { get; }
        
        public string Title { get; }

        public DatabaseViewModel() : this(null, null)
        {

        }
        
        public DatabaseViewModel(FileInfo fileInfo, LiteDatabase database)
        {
            DatabaseFileInfo = fileInfo;
            Database = database;
            Filepath = fileInfo?.FullName ?? UnknownValueString;
            Title = fileInfo?.Name ?? UnknownValueString;

            Properties = new ObservableCollection<DbPropertyTreeNodeViewModel>();
            CloseDatabaseCmd = ReactiveCommand.Create(CloseDatabase); 
            RefreshProperties();
        }

        public void CloseDatabase()
        {
            DatabaseViewModelService.CloseDatabase(this);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                Database?.Dispose();
            }

            _disposed = true;
        }
        
        private FileInfo DatabaseFileInfo { get; }

        public void RefreshProperties()
        {
            Properties.Clear();
            GenerateSystemsSettingsChildren();
            BuildCollections();
        }

        private void GenerateSystemsSettingsChildren()
        {
            const string systemsNodeName = "System";
            if (Properties.Any(p => p.Text == systemsNodeName))
            {
                return;
            }
            
            var settings = new List<string>
            {
                "$cols", "$database", "$dump", "$dump_cache", "$dump_log", "$indexes", "$sequences", "$snapshots",
                "$transactions"
            }.Select(s => new DbPropertyTreeNodeViewModel(s, IconGenerator.SystemSettingsIcon, s){
                HasSelectMenu = true,
                HasMenus = true,
                ParentViewModel = this
            });
            var systemTreeNodes = new DbPropertyTreeNodeViewModel("System", IconGenerator.FolderIcon)
            {
                ParentViewModel = this
            };
            systemTreeNodes.Children.AddRange(settings);
            Properties.Add(systemTreeNodes);
        }
        private void BuildCollections()
        {
            var collections = Database?.GetCollectionNames()?.ToList()
                .Select(c => new DbPropertyTreeNodeViewModel(c, IconGenerator.TableIcon, c){
                    HasSelectMenu = true,
                    HasDistinctMenu = true,
                    HasCountMenu = true,
                    HasIndexesMenu = true,
                    HasDeleteMenu = true,
                    HasExplainMenu = true,
                    HasInsertMenu = true,
                    HasRenameMenu = true,
                    HasDropCollectionMenu = true,
                    HasMenus = true,
                    ParentViewModel = this
                });

            if (collections == null)
            {
                return;
            }

            const string collectionsName = "Collections";
            var collectionTreeNode = Properties.FirstOrDefault(p => p.Text == collectionsName);

            if (collectionTreeNode == null)
            {
                collectionTreeNode = new DbPropertyTreeNodeViewModel("Collections", IconGenerator.TableIcon)
                {
                    HasInsertMenu = true,
                    HasMenus = true,
                    ParentViewModel = this
                };
                Properties.Add(collectionTreeNode);
            }

            collectionTreeNode.Children.Clear();
            collectionTreeNode.Children.AddRange(collections);

        }

    }
}