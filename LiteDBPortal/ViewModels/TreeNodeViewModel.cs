using System.Collections.ObjectModel;
using Avalonia.Media.Imaging;

namespace LiteDBPortal.ViewModels
{
    public class TreeNodeViewModel : ViewModelBase
    {
        
        public object NativeData { get; }
        
        public string Text { get; }
        
        public Bitmap Icon { get; }

        public ObservableCollection<TreeNodeViewModel> Children { get; }

        private TreeNodeViewModel()
        {

        }

        public TreeNodeViewModel(string text, Bitmap icon, object nativeData = default)
        {
            Text = text;
            Icon = icon;
            NativeData = nativeData;
            Children = new ObservableCollection<TreeNodeViewModel>();
        }
        
    }
}