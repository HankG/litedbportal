using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace LiteDBPortal.Services
{
    public class QueryParsingService
    {
        public const long MaxResultsPerPage = 2000;
        
        public static bool QueryMayChangeDatabase(string queryText)
        {
            var query = queryText.ToUpperInvariant().Trim();
            return query.StartsWith("INSERT") || query.StartsWith("DELETE") || query.StartsWith("UPDATE") ||
                   query.StartsWith("RENAME") || query.Contains("FROM $FILE_JSON") || query.StartsWith("DROP") ||
                   query.StartsWith("RENAME");
        }

        public static string ExtractCollectionName(string originalQuery)
        {
            const string fromField = " FROM ";
            var query = originalQuery.Trim().Trim(';');
            var fromIndex = query.IndexOf(fromField, StringComparison.InvariantCultureIgnoreCase);
            var fields = Regex.Split(query.Substring(fromIndex + fromField.Length), "\\s+")
                .Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
            var collectionName = fields.Count > 0 ? fields[0] : "";
            return collectionName;
        }
        
        public static string GenerateQueryWithPageOffsetLimit(string initialQuery, long offset, long maxResults)
        {
            if (!IsPureSelectQuery(initialQuery))
            {
                return initialQuery;
            }

            var queryToProcess = initialQuery.Trim().TrimEnd(';');
            var max = maxResults > MaxResultsPerPage ? MaxResultsPerPage : maxResults;
            var actualOffset = offset < 0 ? 0 : offset;
            var limitPage = $" LIMIT {max} OFFSET {actualOffset}";
            var queryOnly = ExtractCoreQuery(queryToProcess);
            var rval = queryOnly + limitPage;
            return rval;
        }
        
        public static string GenerateQueryWithMinMax(string initialQuery, long min, long max)
        {
            var delta = Math.Abs(max - min);
            var limit = delta > MaxResultsPerPage ? MaxResultsPerPage : delta;
            var offset = min < max ? min : max;
            return GenerateQueryWithPageOffsetLimit(initialQuery, offset, limit);
        }

        private static string ExtractCoreQuery(string originalQuery)
        {
            var whereIndex = originalQuery.LastIndexOf(" WHERE ", StringComparison.InvariantCultureIgnoreCase);
            if (whereIndex < 0)
            {
                whereIndex = 0;
            }

            var characterCount = originalQuery.Length - whereIndex;
            const string offsetKey = "OFFSET ";
            const string limitKey = "LIMIT ";
            var offsetIndex = originalQuery.LastIndexOf(offsetKey, originalQuery.Length, characterCount, StringComparison.InvariantCultureIgnoreCase);
            var limitIndex = originalQuery.LastIndexOf(limitKey, originalQuery.Length, characterCount, StringComparison.InvariantCultureIgnoreCase);
            
            var reallyHasOffset = offsetIndex >= 0 && IsActualLimitOrOffsetField(offsetKey, originalQuery, offsetIndex);
            var reallyHasLimit = limitIndex >= 0 && IsActualLimitOrOffsetField(limitKey, originalQuery, limitIndex);

            if (!reallyHasLimit && !reallyHasOffset)
            {
                return originalQuery;
            }

            if (!reallyHasLimit)
            {
                limitIndex = -1;
            }

            if (!reallyHasOffset)
            {
                offsetIndex = -1;
            }
            

            if (offsetIndex > 0 && limitIndex > 0)
            {
                var substringOffset = offsetIndex < limitIndex ? offsetIndex : limitIndex;
                return originalQuery.Substring(0, substringOffset).Trim();
            }

            if (offsetIndex < 0)
            {
                return originalQuery.Substring(0, limitIndex).Trim();
            }

            return originalQuery.Substring(0, offsetIndex).Trim();
        }
        
        private static bool IsActualLimitOrOffsetField(string field, string originalString, int offset)
        {
            var index = offset + field.Length;
            for (var i = index; i < originalString.Length; i++)
            {
                if (char.IsWhiteSpace(originalString[i]) || char.IsControl(originalString[i]))
                {
                    continue;
                }

                return char.IsDigit(originalString[i]);
            }

            return false;
        }

        private static bool IsPureSelectQuery(string query)
        {
            var fields = Regex.Split(query.Trim(), "\\s+")
                .Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            if (query.Contains("$FILE_JSON", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }
            
            if (fields.Count == 0)
            {
                return false;
            }

            if (!fields[0].Equals("SELECT", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            return !fields[1].StartsWith("DISTINCT", StringComparison.InvariantCultureIgnoreCase) 
                   && !fields[1].StartsWith("COUNT");
        }
    }
}